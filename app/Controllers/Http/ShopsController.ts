import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Shop from 'App/Models/Shop'
import Cloudinary from 'App/Services/Cloudinary'



export default class ShopsController {
  public async index({ response }: HttpContextContract) {

    try {
      const shops = await Shop.all()

      response.status(200).json({
        shops
      })

    } catch (error) {
      response.status(500).json({
        message: "Could not fetch shops",
        error: error
      })

    }
  }
  public async store({ request, response }: HttpContextContract) {


    const cloudinary_response = await Cloudinary.upload(request.file('image'))

    const user_id = request.input('user_id')
    const name = request.input('name')
    const description = request.input('description')
    const location = request.input('location')

    try {
      const shop = await Shop.create({
        name: name,
        user_id: user_id,
        description: description,
        image: cloudinary_response.url,
        location: location
      })
      console.log(shop)

      return response.status(200).json({
        shop
      })
    }
    catch (error) {
      return response.status(500).json({
        message: "Could not add shop",
        error: error
      })
    }
  }


  public async show({ params, response }: HttpContextContract) {
    const shop = await Shop.find(params.id)
    try {
      return response.status(200).json({
        shop
      })
    } catch (error) {
      return response.status(500).json({
        message: "Could not retrieve shop",
        error: error
      })
    }


  }

  public async update({ request, params, response }: HttpContextContract) {
    const shop = await Shop.find(params.id)
    const user_id = request.input('user_id')
    const name = request.input('name')

    try {
      if (shop) {
        shop.name = name;
        shop.user_id = user_id;
        await shop.save()
        return response.status(200).json({
          message: "successfully updated shop's details",
          data: shop
        })
      }

    } catch (error) {
      return response.status(500).json({
        message: "Could not update shop details",
        error: error
      })

    }
  }

  public async destroy({ params, response }: HttpContextContract) {
    const shop = await Shop.find(params.id)
    try {
      await shop?.delete()
      return response.status(200).json({
        message: "successfully deleted shop",

      })
    } catch (error) {
      return response.status(500).json({
        message: "Could not delete shop",
        error: error
      })

    }

  }
}
