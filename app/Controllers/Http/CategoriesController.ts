import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Category from 'App/Models/Category'

export default class CategoriesController {
  public async index({ response, auth }: HttpContextContract) {
    await auth.use('api').authenticate()
    try {
      const categories = await Category.all()

      response.status(200).json({
        message: "All Categoriess",
        data: categories
      })

    } catch (error) {
      return response.status(500).json({
        message: "Could not fetch categories",
        error: error
      })

    }
  }
  public async store({ request, response }: HttpContextContract) {
    const description = request.input('description')
    const name = request.input('name')
    const image = request.input('image')
    try {
      const category = await Category.create({
        name,
        description,
        image

      })
      console.log(category)

      return response.status(200).json({
        message: "successfully added category",
        data: category
      })
    }
    catch (error) {
      return response.status(500).json({
        message: "Could add a category",
        error: error
      })

    }
  }


  public async show({ params, response }: HttpContextContract) {
    const category = await Category.find(params.id)

    try {
      return response.status(200).json({
        message: "Category:",
        data: category
      })
    } catch (error) {
      return response.status(500).json({
        message: "Could not retrieve category",
        error: error
      })

    }


  }

  public async update({ request, params, response }: HttpContextContract) {
    const category = await Category.find(params.id)
    const description = request.input('description')
    const name = request.input('name')
    const image = request.input('image')

    try {
      if (category) {
        category.name = name;
        category.description = description;
        category.image = image;
        await category.save()
        return response.status(200).json({
          message: "successfully updated category's details",
          data: category
        })
      }
    } catch (error) {
      return response.status(500).json({
        message: "Could not update category details",
        error: error
      })

    }


  }

  public async destroy({ params, response }: HttpContextContract) {
    const category = await Category.find(params.id)
    try {
      await category?.delete()
      return response.status(200).json({
        message: "successfully deleted category",

      })

    } catch (error) {
      return response.status(500).json({
        message: "Could not delete category",
        error: error
      })

    }

  }
  public async getCategory({ response }: HttpContextContract) {

    try {
      const categories = await Category
        .query()
        .preload('sub_categories')

      categories.forEach((user) => {
        console.log(user.sub_categories)
      })

      response.status(200).json({
       
        categories
      })
    } catch (error) {
      return response.status(500).json({
        message: "Could not retrieve category",
        error: error
      })

    }

  }
}
