import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import SubCategory from 'App/Models/SubCategory'
import Cloudinary from 'App/Services/Cloudinary'

export default class CategoriesController {
  public async index({ response }: HttpContextContract) {
    try {
      const subcategories = await SubCategory.all()

      response.status(200).json({
        message: "All Subcategories",
        data: subcategories
      })
    } catch (error) {
      return response.status(500).json({
        message: "Could not retrieve subcategories",
        error: error
      })

    }


  }
  public async store({ request, response }: HttpContextContract) {
    const cloudinary_response = await Cloudinary.upload(request.file('image'))
    const description = request.input('description')
    const name = request.input('name')
    const categoryId = request.input('category_id')

    try {
      const subcategory = await SubCategory.create({
        name,
        description,
        image: cloudinary_response.url,
        categoryId
      })
      return response.status(200).json({
        message: "successfully added subcategory",
        data: subcategory
      })
    } catch (error) {
      return response.status(500).json({
        message: "Could not add subcategory",
        error: error
      })

    }

  }

  public async show({ params, response }: HttpContextContract) {
    const subcategory = await SubCategory.find(params.id)
    try {
      return response.status(200).json({
        message: "SubCategory:",
        data: subcategory
      })

    } catch (error) {
      return response.status(500).json({
        message: "Could not retrieve subcategory",
        error: error
      })

    }
  }

  public async update({ request, params, response }: HttpContextContract) {
    const subcategory = await SubCategory.find(params.id)
    const description = request.input('description')
    const name = request.input('name')
    const image = request.input('image')

    try {
      if (subcategory) {
        subcategory.name = name;
        subcategory.description = description;
        subcategory.image = image;
        await subcategory.save()
        return response.status(200).json({
          message: "successfully updated subcategory's details",
          data: subcategory
        })
      }
    } catch (error) {
      return response.status(500).json({
        message: "Could not update subcategory",
        error: error
      })

    }
  }

  public async destroy({ params, response }: HttpContextContract) {
    const subcategory = await SubCategory.find(params.id)
    try {
      await subcategory?.delete()
      return response.status(200).json({
        message: "successfully deleted subcategory",
      })
    } catch (error) {
      return response.status(500).json({
        message: "Could not delete subcategory",
        error: error
      })

    }

  }
}
