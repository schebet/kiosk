import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'
import CreateUserValidator from 'App/Validators/CreateUserValidator'

export default class UsersController {
    public async users({ request, response }: HttpContextContract) {
        const users = await User.all()

        return response.status(200).json({
            users
        })

    }
    public async register({ request, auth, response }: HttpContextContract) {
        const data = await request.validate(CreateUserValidator)


        const email = data.email
        const password = data.password
        const phone = data.phone
        const role = data.role
        const name = data.name

        const user = await User.create({
            email: email,
            phone: phone,
            name: name,
            password: password,

        })
        const user_role = await (await user).related('roles').create({
            name: role
        })
        const token = await auth.use('api').attempt(email, password)
        response.status(200).json({
            user: token.user,
            token: token
        })
    }

    public async login({ request, response, auth }: HttpContextContract) {

        const email = request.input('email')
        const password = request.input('password')
        try {
            const token = await auth.use('api').attempt(email, password)
            return response.status(200).json({
                user: token.user,
                token: token
            })
        } catch {
            return response.badRequest('Wrong Credentials')

        }
    }
    public async logout({ auth, response }) {
        try {
            await auth.use('api').revoke
            return {
                revoked: true
            }

        } catch (error) {
            return response.status(500).json({
                message: "Logout not successful",
                error: error
            })

        }

    }

}