import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Product from 'App/Models/Product'
import Cloudinary from 'App/Services/Cloudinary'

export default class CategoriesController {
    public async index({ response }: HttpContextContract) {
        try {
            const categories = await Product.all()
            response.status(200).json({
                message: "All Products",
                data: categories
            })

        } catch (error) {
            return response.status(500).json({
                message: "Could not fetch products",
                error: error
            })

        }

    }
    public async store({ request, response }: HttpContextContract) {
        try {
            const cloudinary_response = await Cloudinary.upload(request.file('image'))

            const description = request.input('description')
            const name = request.input('name')
            const sub_category_id = request.input('sub_category_id')
            const price = request.input('price')
            const shop_id = request.input('shop_id')
            const quantity = request.input('quantity')

            const product = await Product.create({
                name,
                description,
                image: cloudinary_response.url,
                sub_category_id,
                price,
                shop_id

            })

            await (await product).related('inventory').create({
                quantity,
                productId: product.id
            })

            return response.status(200).json({
                message: "successfully added product",
                data: product
            })
        } catch (error) {
            return response.status(500).json({
                message: "Could not add product",
                error: error
            })

        }

    }

    public async show({ params, response }: HttpContextContract) {

        try {
            const product = await Product.find(params.id)

            return response.status(200).json({
                message: "Product:",
                data: product
            })

        } catch (error) {
            return response.status(500).json({
                message: "Could not retrieve shop",
                error: error
            })
        }
    }

    public async update({ request, params, response }: HttpContextContract) {

        try {
            const product = await Product.find(params.id)
            const description = request.input('description')
            const name = request.input('name')
            const image = request.input('image')
            const sub_category_id = request.input('sub_category_id')
            const price = request.input('price')
            const shop_id = request.input('shop_id')

            if (product) {
                product.name = name;
                product.description = description;
                product.image = image;
                product.sub_category_id = sub_category_id;
                product.price = price;
                product.shop_id = shop_id;

                await product.save()
                return response.status(200).json({
                    message: "successfully updated product's details",
                    data: product
                })
            }

        } catch (error) {
            return response.status(500).json({
                message: "Could not update product",
                error: error
            })
        }
    }

    public async destroy({ params, response }: HttpContextContract) {
        try {
            const product = await Product.find(params.id)
            await product?.delete()

            return response.status(200).json({
                message: "Successfully deleted product",
            })

        } catch (error) {
            return response.status(500).json({
                message: "Could not delete product",
                error: error
            })
        }
    }

    public async getProductsbySubCategory({ params, response }: HttpContextContract) {

        try {
            const products = await Product.findBy('sub_category_id', params.id)
            return response.status(500).json({
                message: "Products",
                data: products
            })

        } catch (error) {
            return response.status(500).json({
                message: "Could not retrieve products",
                error: error
            })

        }

    }

    public async getProductsbyShop({ params, response }: HttpContextContract) {
        try {
            const shop = params.shop;
            const sub_category = params.sub_category;
            console.log(shop);


            const products = await Product
                .query()
                .where('shop_id', '=', shop)
                .andWhere('sub_category_id', '=', sub_category)

            return response.status(200).json({
                products
            })
        } catch (error) {
            return response.status(500).json({
                message: "Could not retrieve products",
                error: error
            })

        }
    }
    public async searchByTerm({ request, response }: HttpContextContract) {
        try {
            const term = request.input('term');
            let products = await Product.query().where('name', 'like', '%' + term + '%')
            .orWhere('description', 'like', '%' + term + '%')

              return response.json({
                products
            })


        } catch (error) {
            return response.json({
                error
            })

        }

    }

}
