import { DateTime } from 'luxon'
import {
  BaseModel, column, hasOne,
  HasOne, belongsTo, BelongsTo,

} from '@ioc:Adonis/Lucid/Orm'

import Inventory from 'App/Models/Inventory'
import Shop from 'App/Models/Shop'
import SubCategory from 'App/Models/SubCategory'

export default class Product extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string
  
  @column()
  public image: string

  @column()
  public description: string

  @column()
  public sub_category_id: number

  @column()
  public subCategoryId: number

  @column()
  public shop_id: number

  @column()
  public price: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @hasOne(() => Inventory)
  public inventory: HasOne<typeof Inventory>

  @belongsTo(() => Shop)
  public shop: BelongsTo<typeof Shop>

  @belongsTo(() => SubCategory)
  public subcategory: BelongsTo<typeof SubCategory>
}
