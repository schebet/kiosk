import { DateTime } from 'luxon'
import {
  BaseModel, column, hasMany, HasMany, hasManyThrough,
  HasManyThrough
} from '@ioc:Adonis/Lucid/Orm'
import Product from './Product'
import SubCategory from './SubCategory'


export default class Category extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column()
  public image: string

  @column()
  public description: string


  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime


  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @hasMany(() => SubCategory)
  public sub_categories: HasMany<typeof SubCategory>

}
