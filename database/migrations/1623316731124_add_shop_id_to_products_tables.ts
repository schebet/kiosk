import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class AlterUsersAddUserIds extends BaseSchema {
  protected tableName = 'products'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.integer('shop_id').unsigned().references('id').inTable('shops').onDelete('CASCADE')

    })
  }

  public async down () {
    this.schema.alterTable(this.tableName, (table) => {
      table.dropColumn('shop_id')

    })  }
}
