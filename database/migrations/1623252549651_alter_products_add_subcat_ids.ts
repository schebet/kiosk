import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class AlterUsersAddUserIds extends BaseSchema {
  protected tableName = 'products'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.integer('subcategory_id').unsigned().references('id').inTable('sub_categories').onDelete('CASCADE')

    })
  }

  public async down () {
    this.schema.alterTable(this.tableName, (table) => {
      table.dropColumn('subcategory_id')

    })  }
}
