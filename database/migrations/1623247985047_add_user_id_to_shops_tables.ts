import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class AlterUsersAddUserIds extends BaseSchema {
  protected tableName = 'shops'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.integer('user_id').unsigned().references('id').inTable('users').onDelete('CASCADE')

    })
  }

  public async down () {
    this.schema.alterTable(this.tableName, (table) => {
      table.dropColumn('user_id')

    })  }
}
