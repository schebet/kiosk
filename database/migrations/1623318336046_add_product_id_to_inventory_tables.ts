import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class AlterUsersAddUserIds extends BaseSchema {
  protected tableName = 'inventory'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.integer('product_id').unsigned().references('id').inTable('products').onDelete('CASCADE')

    })
  }

  public async down () {
    this.schema.alterTable(this.tableName, (table) => {
      table.dropColumn('product_id')

    })  }
}
