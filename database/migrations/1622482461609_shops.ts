import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Shops extends BaseSchema {
  protected tableName = 'shops'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('name')
      
      table.timestamp('updatedAt', { useTz: true }).nullable().defaultTo(this.now())
      table.timestamp('createdAt', { useTz: true }).notNullable().defaultTo(this.now())
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
