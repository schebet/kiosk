/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return { hello: 'world' }
})

//Authentication
Route.post("register", async (ctx) => {
  const { default: UsersController } = await import(
    "App/Controllers/Http/UsersController"
  );
  return new UsersController().register(ctx);
});
Route.post('login', 'UsersController.login')

Route.post("logout", async (ctx) => {
  const { default: UsersController } = await import(
    "App/Controllers/Http/UsersController"
  );
  return new UsersController().logout(ctx);
});

Route.get('users', 'UsersController.users')

//shops

Route.get('shops', 'ShopsController.index')
Route.get('shops/:id', 'ShopsController.show')
Route.delete('shops/:id', 'ShopsController.destroy')
Route.put('shops/:id', 'ShopsController.update')
Route.post('shops', 'ShopsController.store')

//categories

Route.get('categories', 'CategoriesController.index')
Route.get('categories/:id', 'CategoriesController.show')
Route.delete('categories/:id', 'CategoriesController.destroy')
Route.put('categories/:id', 'CategoriesController.update')
Route.post('categories', 'CategoriesController.store')
Route.get('categories/category/subcategories', 'CategoriesController.getCategory')

//subcategories

Route.get('subcategories', 'SubcategoriesController.index')
Route.get('subcategories/:id', 'SubcategoriesController.show')
Route.delete('subcategories/:id', 'SubcategoriesController.destroy')
Route.put('subcategories/:id', 'SubcategoriesController.update')
Route.post('subcategories', 'SubcategoriesController.store')

//products
Route.get('products', 'ProductsController.index')
Route.get('products/:id', 'ProductsController.show')
Route.delete('products/:id', 'ProductsController.destroy')
Route.put('products/:id', 'ProductsController.update')
Route.post('products', 'ProductsController.store')
Route.get('products/:sub_category/:shop', 'ProductsController.getProductsbyShop')
Route.post('products/search', 'ProductsController.searchByTerm')



Route.get('products/getBySubCategory/:id', 'ProductsController.getProductsbySubCategory');